import moment from 'moment';
export const getCurrentDate = (format) => {
    return moment().format(format);
};
