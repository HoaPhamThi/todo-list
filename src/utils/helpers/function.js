import moment from 'moment';

export const getItemFromLocalStorage = (key) => {
    const value = localStorage.getItem(key);
    return JSON.parse(value);
}

export const saveItemToLocalStorage = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
}

export const sortByDueDate = (array) => {
    return array.sort((a,b) => new moment(a.dueDate) - new moment(b.dueDate))
}

export const randomString = () => {
    return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(2, 10);
}