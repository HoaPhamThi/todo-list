import { useState } from "react";
import Input from "../common/Input";
import DatePicker from "../common/DatePicker";
import Select from "../common/Select";
import Button from "../common/Button";
import { msg } from "../../utils/constants";
import { dateHelper} from "../../utils/helpers"

const TaskFormComponent = (props) => {
    const { type, task, buttonText, handleOnClick } = props;

    const [formValue, setFormValue] = useState(
        {
            title: task?.title ?? "",
            des: task?.des ?? "",
            dueDate: task?.dueDate ?? dateHelper.getCurrentDate(),
            priority: task?.priority ?? "normal",
        }
    );
    const { title, des, dueDate, priority } = formValue;

    const handleOnChange = (tag, value) => {
        console.log(value);
        setFormValue(preState => ({
            ...preState,
            [tag]: value
        }))
    }

    return (
        <div className="components__form-task margin-top-50">
            <div className="formtask__title">
                <Input
                    type="text"
                    placeholder={title !== "" ? "" : "Add new task ..."}
                    value={title}
                    onChange={(title) => handleOnChange("title", title)}
                />
                <span className={`input__validation--empty input__validation--empty${task?.taskId} display-none`}>{msg.validate.title_empty}</span>
            </div>
            <div className="formtask__des margin-top-20">
                <p className="margin-bt-5">Description</p>
                <Input
                    type="textarea"
                    placeholder={des}
                    value={des}
                    onChange={(des) => handleOnChange("des", des)}
                />
            </div>
            <div className="option__wrap display-flex justify-content-space-between margin-top-20">
                <div className="formtask__due-date">
                    <p className="margin-bt-5">Due date</p>
                    <DatePicker
                        type="datepicker"
                        selected={dueDate === '' ? new Date() : new Date(dueDate)}
                        handleOnChange={(dueDate) => handleOnChange("dueDate", dueDate)}
                        dateFormat="yyyy-MM-dd"
                    />
                </div>
                <div className="formtask__priority">
                    <p className="margin-bt-5">Priority</p>
                    <Select
                        onChange={(priority) => handleOnChange("priority", priority)}
                        value={priority}
                        options={[
                            {
                                value: "low",
                                label: "Low",
                            },
                            {
                                value: "normal",
                                label: "Normal",
                            },
                            {
                                value: "high",
                                label: "High",
                            },
                        ]} />
                </div>
            </div>

            <Button
                className="btn__formtask margin-top-50 margin-bt-30 btn__bgr--green"
                buttonText={buttonText}
                onClick={() => handleOnClick(type, formValue, task?.taskId)} />
        </div>
    );
}

export default TaskFormComponent;
