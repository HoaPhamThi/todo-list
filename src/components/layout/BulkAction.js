import Button from '../common/Button';
const BulkActionComponent = (props) => {
    const { checkboxList, handleRemoveMultiple } = props;

    return (
        <div className="components__bulk-action display-none display-flex justify-content-space-between align-items-center ">
            <p className="text-center">Bulk Action:</p>
            <div className="bulk-action__btn-wrap display-flex">
                <Button className="btn__size-md btn__bgr--blue" buttonText="Done" />
                <Button
                    className="btn__size-md btn__bgr--red margin-left-20"
                    buttonText="Remove"
                    onClick = {()=>handleRemoveMultiple(checkboxList)}
                />
            </div>
        </div>
    )
}

export default BulkActionComponent;