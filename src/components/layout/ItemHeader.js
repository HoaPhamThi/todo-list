import { useEffect, useState } from 'react';
import Button from '../common/Button';

const ItemHeaderComponent = (props) => {
    const { task, handleClickRemoveSingle, handleOnChangeCheckboxList } = props;
    const [taskChecked, setTaskChecked] = useState({
        value: "",
        checked: false,
    });

    const handleClickViewDetail = () => {
        const elm = document.querySelector('.todo__details' + task.taskId);
        elm.classList.toggle('display-none');
    }
    const onChangeCheckboxValues = (value) => {
        setTaskChecked({
            value: value,
            checked: !taskChecked.checked
        })
    }
    useEffect(() => {
        handleOnChangeCheckboxList(taskChecked)
    }, [onChangeCheckboxValues])
    return (
        <div className="components__item-header display-flex justify-content-space-between align-items-center">
            <div className="item-header__checkbox display-flex align-items-center">
                <input
                    type="checkbox"
                    name="task.taskId"
                    id={`checkbox${task?.taskId}`}
                    value={task?.taskId}
                    label={task?.title}
                    checked={taskChecked?.checked}
                    onChange={(event) => {
                        onChangeCheckboxValues(event?.target?.value);
                        handleOnChangeCheckboxList(taskChecked)
                    }}
                />
                <label htmlFor={`checkbox${task?.taskId}`} >{task?.title}</label>
            </div>

            <div className="item-header__btn-wrap display-flex justify-content-space-between">
                <Button className="btn__bgr--blue" buttonText="Detail" onClick={() => handleClickViewDetail()} />
                <Button className="btn__bgr--red margin-left-20" buttonText="Remove" onClick={() => handleClickRemoveSingle(task?.taskId)} />
            </div>
        </div>
    )
}

export default ItemHeaderComponent;