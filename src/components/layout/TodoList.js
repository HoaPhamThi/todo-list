import Input from '../common/Input';
import TodoItemComponent from './TodoItem';
import BulkActionComponent from './BulkAction';
import { useEffect, useState } from 'react';
import { functionHelper } from '../../utils/helpers';

const TodoListComponent = (props) => {
    const { taskList, handleClickRemoveSingle, handleOnClickAddUpdate, handleOnChangeCheckboxList, checkboxList, handleRemoveMultiple } = props;
    const [searchValue, setSearchValue] = useState('');
    const [taskListFinal, setTaskListFinal] = useState([]);

    useEffect(() => {
        const sortList = functionHelper.sortByDueDate(taskList);
        setTaskListFinal(sortList);
    },[taskList])

    const handleOnChangeSearch = (value) => {
        setSearchValue(value)
        let searchList = taskList.filter((task) => {
            return task.title.toLowerCase().includes(value.toLowerCase())
        })
        searchList = functionHelper.sortByDueDate(searchList);
        setTaskListFinal(searchList);
    }

    return (
        <div className="components__right">
            <div className="todo-list__wrap">
                <h3 className="right__title text-center">Todo List</h3>
                <div className="todo__search margin-top-50">
                    <Input
                        type="text"
                        placeholder="Search..."
                        value={searchValue}
                        onChange={(value) => handleOnChangeSearch(value)}
                    />
                </div>
                <div className="todo__list margin-top-20">
                    {
                        taskListFinal?.map((task, index) =>
                            <TodoItemComponent
                                task={task}
                                key={index}
                                handleClickRemoveSingle={handleClickRemoveSingle}
                                handleOnClickAddUpdate={handleOnClickAddUpdate}
                                handleOnChangeCheckboxList={handleOnChangeCheckboxList}
                            />
                        )
                    }
                </div>
            </div>

            <BulkActionComponent checkboxList={checkboxList} handleRemoveMultiple={handleRemoveMultiple}/>
        </div>
    );
}

export default TodoListComponent;
