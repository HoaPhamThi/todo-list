import TaskFormComponent from "../form/TaskForm";
import ItemHeaderComponent from "./ItemHeader";

const TodoItemComponent = (props) => {
    const { task, handleClickRemoveSingle, handleOnClickAddUpdate, handleOnChangeCheckboxList} = props;

    return (
        <div className="components__todo-item margin-bt-30">
            <div className="todo__header">
                <ItemHeaderComponent 
                task={task} 
                handleClickRemoveSingle={handleClickRemoveSingle} 
                handleOnChangeCheckboxList={handleOnChangeCheckboxList}
                />
            </div>
            <div className={`todo__details todo__details${task.taskId} display-none`}>
                <TaskFormComponent
                    type="update"
                    task={task}
                    buttonText="Update"
                    handleOnClick={handleOnClickAddUpdate}
                />
            </div>
        </div>
    )
}

export default TodoItemComponent;