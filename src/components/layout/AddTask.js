import TaskFormComponent from "../form/TaskForm";

const AddTaskComponent = (props) => {
    const {handleOnClickAddUpdate} = props;
    return (
        <div className="components__left">
            <h3 className="left__title text-center">New Task</h3>
            <TaskFormComponent 
            type="add" 
            buttonText="Add" 
            handleOnClick={handleOnClickAddUpdate}/>
        </div>
    );
}

export default AddTaskComponent;
