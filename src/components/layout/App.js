import { useEffect, useState } from 'react';
import AddTaskComponent from './AddTask';
import TodoListComponent from './TodoList';
import { functionHelper } from '../../utils/helpers';

const App = () => {
    const [taskList, setTaskList] = useState([]);
    const [todolist, setTodolist] = useState([]);
    const [changeList, setChangeList] = useState(false);
    const [checkboxList, setCheckboxList] = useState([]);

    useEffect(() => {
        setTaskList(functionHelper.getItemFromLocalStorage("taskList") ?? []);
    }, [])

    useEffect(() => {
        setTodolist(taskList)
    }, [changeList, taskList])

    const storeValue = (value) => {
        setTaskList(value);
        functionHelper.saveItemToLocalStorage("taskList", value);
        setChangeList(!changeList);
    }

    const handleOnClickAddUpdate = (type, formValue, taskId) => {
        let listValue = taskList;
        const elm = document.querySelector('.input__validation--empty' + taskId);
        if (formValue.title === '') {
            elm.classList.remove('display-none');
        } else {
            if (type === "add") {
                const taskId = functionHelper.randomString();
                formValue = { ...formValue, taskId: taskId };
                listValue.unshift(formValue);
            } else if (type === "update") {
                formValue = { ...formValue, taskId: taskId };
                listValue = listValue.map(item => taskId === item.taskId ? formValue : item
                )
            }
            storeValue(listValue);
            elm.classList.add('display-none');
        }
    }

    const handleClickRemoveSingle = (taskId) => {
        const removeList = taskList.filter(item => item.taskId !== taskId)
        storeValue(removeList)
    }

    const handleOnChangeCheckboxList = (checkboxValue) => {
        const list = checkboxList;
        const duplicateValue = list.find(item => item['value'] === checkboxValue.value);

        if (duplicateValue === undefined) {
            list.push(checkboxValue);
        } else if (duplicateValue.value !== '') {
            list.map((item) => item === duplicateValue ? item.checked = !duplicateValue.checked : null)
        }
        setCheckboxList(list);
        const elm = document.querySelector('.components__bulk-action');
        const isDisplay = document.querySelectorAll('input[type=checkbox]:checked')
        isDisplay.length > 0 ? elm.classList.remove('display-none') : elm.classList.add('display-none');
    }

    const handleRemoveMultiple = (checkboxList) => {
        let listFilter = checkboxList.filter((item) => {
            return item.value !== '' && item.checked === true ? item : null
        })
        let removeList = taskList;
        listFilter.forEach(item => {
            removeList = removeList.filter(removeItem => removeItem.taskId !== item.value)
        })
        document.querySelectorAll('input[type=checkbox]').forEach(el => el.checked = false);
        storeValue(removeList);
    }

    return (
        <div className="components__app">
            <AddTaskComponent
                handleOnClickAddUpdate={handleOnClickAddUpdate}
            />
            <TodoListComponent
                taskList={todolist}
                handleClickRemoveSingle={handleClickRemoveSingle}
                handleOnClickAddUpdate={handleOnClickAddUpdate}
                handleOnChangeCheckboxList={handleOnChangeCheckboxList}
                checkboxList={checkboxList}
                handleRemoveMultiple={handleRemoveMultiple}
            />
        </div>
    );
}

export default App;
