import { createRef } from 'react';
import Img from './Img';

const Button = (props) => {
    const {
        buttonText,
        className,
        onClick,
        disabled,
        textClassName,
        contentMode,
    } = props;
    const btn = createRef();

    const handleClickButton = (event) => {
        const { pageX, pageY, currentTarget } = event;

        const rect = currentTarget.getBoundingClientRect();
        const left = pageX - (rect.left + window.scrollX);
        const top = pageY - (rect.top + window.scrollY);
        if (!disabled) {
            const ripples = document.createElement('span');
            ripples.style.left = left + 'px';
            ripples.style.top = top + 'px';
            ripples.classList.add('components__button-ripple');
            btn?.current?.appendChild(ripples);

            const timeout = setTimeout(() => {
                clearTimeout(timeout);
                ripples.remove();
            }, 900);

            if (onClick) {
                onClick();
            }
        }
    };

    return (
        <button
            ref={btn}
            className={`components__button ${className} ${
                disabled ? 'components__button_disable' : ''
            } ${contentMode === 'wrap' ? 'components__button_content-wrap' : ''}`}
            onClick={(event) => handleClickButton(event)}
            disabled={disabled}>
            <div className={`${textClassName} components__button-text`}>{buttonText}</div>
        </button>
    );
};

Button.defaultProps = {
    className: '',
    disabled: false,
    onClick: () => {},
    onEndIconClick: () => {},
    textClassName: '',
    buttonText: '',
    contentMode: 'nowrap',
};

export default Button;
